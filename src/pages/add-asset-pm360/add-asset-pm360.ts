import {Component, ViewChild} from "@angular/core";
import {AlertController, LoadingController, NavController, NavParams} from "ionic-angular";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {ImagePicker} from "@ionic-native/image-picker";
import {File} from "@ionic-native/file";
import {DatabaseProvider} from "../../providers/database/database";
import {AssetListPage} from "../asset-list/asset-list";
import {ApiFactoryProvider} from "../../providers/api-factory/api-factory";
import {LoginPM360Page} from "../login-pm360/login-pm360";


@Component({
  selector: 'page-add-asset-pm360',
  templateUrl: 'add-asset-pm360.html'
})
export class AddAssetPM360Page {

  @ViewChild('assetName') assetName;
  @ViewChild('assetNumber') assetNumber;
  @ViewChild('assetDescription') assetDescription;
  @ViewChild('assetManf') assetManf;
  @ViewChild('assetModel') assetModel;
  @ViewChild('assetSn') assetSn;
  @ViewChild('assetQrText') assetQrText;

  public qrValue: string;
  public photos: any;
  public path: string;
  private rowId: number;
  private roomList: any;
  private assetTypeList: any;
  private loadingView: any;
  private selectedRoom: any;
  private selectedAssetType: any;

  private isRoomLoading: boolean;
  private isAssetTypeLoading: boolean;

  data = {
    rowId: 0,
    asset_description: "",
    asset_area: "",
    asset_manufacturer: "",
    asset_model: "",
    asset_number: "",
    asset_name: "",
    asset_type: "",
    room: "",
    qrValue: "",
    photos: []
  };


  constructor(public navCtrl: NavController,
              private navParams: NavParams,
              private barcode: BarcodeScanner,
              private camera: Camera,
              private apiFactory: ApiFactoryProvider,
              private picker: ImagePicker,
              private database: DatabaseProvider,
              private file: File,
              private loading: LoadingController,
              private alertCtrl: AlertController) {

    this.rowId = this.navParams.get("rowId");

    if (this.rowId) {
      this.showExisting(this.rowId);
    }

    this.database.readToken().then((result: any) => {
      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;

        this.getRooms(accessToken);
        this.getAssetType(accessToken);
      }
    }, (error) => {
      this.navCtrl.push(LoginPM360Page);
    });
  }

  private initLoader() {

    this.loadingView = this.loading.create({
      spinner: 'dots',
      content: 'Please wait...'
    });

  }

  deleteAsset(rowId: number) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to remove this entry?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            alert.dismiss();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.database.deleteData(rowId).then(() => {
              this.log("deleted");
              alert.dismiss();
              this.navCtrl.push(AssetListPage);
            });
          }
        }
      ]
    });
    alert.present();
  }

  ngOnInit() {
    this.photos = [];
    this.roomList = [];
    this.assetTypeList = [];

    this.isAssetTypeLoading = true;
    this.isRoomLoading = true;
  }

  onChange(selectedValue) {
    // alert(this.selectedFloor);
  }

  goToAsset(params) {

    let data = {
      "asset_description": this.assetDescription.value,
      "asset_area": "",
      "asset_manufacturer": this.assetManf.value,
      "asset_model": this.assetModel.value,
      "asset_number": this.assetNumber.value,
      "asset_qr": this.qrValue,
      "asset_name": this.assetName.value,
      "asset_type": this.selectedAssetType,
      "room": this.selectedRoom,
      // "photos": this.photos

    };

    this.saveData(data);
  }

  scanQR(params) {
    this.barcode.scan().then(barcodeData => {
      this.qrValue = barcodeData.text;
    }).catch(err => {
      alert('QR Error: ' + err);
    });
  }

  takePhoto(params) {

    const options: CameraOptions = {
      quality: 100,
      allowEdit: false,
      saveToPhotoAlbum: true,
      correctOrientation: true,
      encodingType: this.camera.EncodingType.JPEG,
      destinationType: this.camera.DestinationType.FILE_URI
    };

    this.camera.getPicture(options).then((imageData) => {
      let filename = imageData.substring(imageData.lastIndexOf('/') + 1);
      let filePath = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      this.log("IMAGE PATH GET PATH. -> " + filePath);

      this.file.readAsDataURL(filePath, filename).then((base64res) => {
          this.photos.push(base64res);
          this.photos.reverse();
        }, (error) => {
          alert(error);
        }
      );
    }, (err) => {
      alert(err);
    });
  }

  pickPhoto(params) {

    let option = {
      title: 'Select Picture',
      message: 'Select Picture',
      maximumImagesCount: 1,
      outType: 0
    };

    this.picker.getPictures(option).then((imageData) => {
      let filename = imageData.substring(imageData.lastIndexOf('/') + 1);
      let filePath = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      this.file.readAsDataURL(filePath, filename).then((base64res) => {
          this.photos.push(base64res);
          this.photos.reverse();
        }
      );
    }, (error) => {

    });
  }

  saveData(data: {}) {
    this.initLoader();
    this.loadingView.present();

    this.database.readToken().then((result: any) => {
      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;
        this.apiFactory.postData(
          "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_asset",
          data,
          accessToken
          , 0).then(
          response => {
            this.loadingView.dismissA();
            this.log("Callback SUCCESS POST")
          },
          error => {
            this.log("Callback ERROR => " + JSON.stringify(error));
            this.saveToDB(data);
          }).then(
          (response) => {
            this.loadingView.dismiss();
            alert("Uploaded Successfully!!!");
          },
          (error) => {
            this.log("in promise error " + JSON.stringify(error));
            //alert("ERROR PROMISE " + JSON.stringify(error));
            this.saveToDB(data);
          });
      }
    }, (error) => {
      this.loadingView.dismiss();
      this.log(JSON.stringify(error));
    });
  }

  private showExisting(rowId: number) {
    // this.initLoader();
    // this.loadingView.present();

    this.database.readDataById("ASSET", rowId).then((result: any) => {
      if (result.rows.length > 0) {

        var row = JSON.parse(result.rows.item(0).data);

        this.data.rowId = row.rowid;
        this.data.asset_description = row.asset_description;
        this.data.asset_area = row.asset_area;
        this.data.asset_manufacturer = row.asset_manufacturer;
        this.data.asset_model = row.asset_model;
        this.data.asset_number = row.asset_number;
        this.data.asset_name = row.asset_name;
        this.data.asset_type = row.asset_type;
        this.data.room = row.room;
        this.qrValue = row.qrValue;
        this.data.photos = row.photos;
        this.photos = row.photos;
      }
      // //this.loadingView.dismissAll();
    }, (error) => {
      // //this.loadingView.dismissAll();
    });
  }

  private getRooms(accessToken: string) {
    // this.initLoader();
    // this.loadingView.present();

    this.isRoomLoading = true;

    this.apiFactory.getData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_room",
      accessToken)
      .then(
        response => JSON.parse(response.data),
        error => {
          this.log("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_room ----- network error" + JSON.stringify(error));
          this.loadOfflineRoom();
          //this.getAssetType();
        })
      .then((response: any) => {

        if (response.result !== undefined) {

          this.roomList = response.result;

          this.database.saveOrReplaceData(JSON.stringify(this.roomList),
            "GET",
            0,
            "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_room",
            "ROOM")
            .then((result) => {
              //this.getAssetType();

              this.log("x_nfgll_pm360_floor saved");
              this.isRoomLoading = false;
            }, (error) => {
              //this.getAssetType();
              this.isRoomLoading = false;
              this.log("failed to save!!!");
            });
        } else {
          this.log("undefined");

          this.loadOfflineRoom();
          //this.getAssetType();
        }

      }, (error) => {
        this.loadOfflineRoom();
        //this.getAssetType();
      })
  }

  private loadOfflineRoom() {
    this.database.readOfflineData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_room", "GET", "ROOM")
      .then((result: any) => {
        if (result.rows.length > 0) {
          this.roomList = JSON.parse(result.rows.item(0).data);
        }
        this.isRoomLoading = false;
      }, (error) => {
        this.log("ERROR " + JSON.stringify(error));
        this.isRoomLoading = false;
      });
  }

  private getAssetType(accessToken: string) {

    this.isAssetTypeLoading = true;

    this.apiFactory.getData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_asset_type",
      accessToken)
      .then(response => JSON.parse(response.data), error => {
        this.loadOfflineAssetType();
      })
      .then((response: any) => {

        if (response.result !== undefined) {
          this.assetTypeList = response.result;

          this.database.saveOrReplaceData(JSON.stringify(this.assetTypeList),
            "GET",
            0,
            "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_asset_type",
            "ASSET TYPE")
            .then((result) => {
              //this.loadingView.dismissAll();
              this.isAssetTypeLoading = false;
              this.log("x_nfgll_pm360_asset_type saved");
            }, (error) => {
              //this.loadingView.dismissAll();
              this.isAssetTypeLoading = false;
              this.log("failed to save!!!");
            });
        } else {
          this.log("undefined");
          this.loadOfflineAssetType();
        }

      }, (error) => {
        this.loadOfflineAssetType();
      })
  }

  private loadOfflineAssetType() {
    this.database.readOfflineData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_asset_type", "GET", "ASSET TYPE")
      .then((result: any) => {
        //this.loadingView.dismissAll();

        if (result.rows.length > 0) {
          this.assetTypeList = JSON.parse(result.rows.item(0).data);
        }
        this.isAssetTypeLoading = false;
      }, (error) => {
        //this.loadingView.dismissAll();
        this.log("ERROR " + JSON.stringify(error));
        this.isAssetTypeLoading = false;
      });
  }

  private saveToDB(data: {}) {
    if (!this.rowId) {
      this.database.saveData(JSON.stringify(data),
        "POST",
        0,
        "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_asset",
        "ASSET")
        .then((result) => {
          this.loadingView.dismiss();
          alert("Saved Successfully!!!");
          this.navCtrl.push(AssetListPage);
        }, (error) => {
          this.loadingView.dismiss();
        });
    } else {
      this.database.updateData(
        JSON.stringify(data),
        "POST",
        0,
        "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_asset",
        "ASSET",
        this.rowId)
        .then((result) => {
          this.loadingView.dismiss();
          alert("Updated Successfully!!!");
          this.navCtrl.push(AssetListPage);
        }, (error) => {
          this.loadingView.dismiss();
        });
    }

  }

  private log(message: string) {
    console.log(message);
  }

}
