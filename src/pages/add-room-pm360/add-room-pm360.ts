import {Component, ViewChild} from "@angular/core";
import {AlertController, LoadingController, NavController, NavParams} from "ionic-angular";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {ImagePicker} from "@ionic-native/image-picker";
import {RoomListPage} from "../room-list/room-list";
import {File} from "@ionic-native/file";
import {DatabaseProvider} from "../../providers/database/database";
import {ApiFactoryProvider} from "../../providers/api-factory/api-factory";
import {LoginPM360Page} from "../login-pm360/login-pm360";

@Component({
  selector: 'page-add-room-pm360',
  templateUrl: 'add-room-pm360.html'
})
export class AddRoomPM360Page {

  @ViewChild('room_name') roomName;
  @ViewChild('room_area') roomArea;
  @ViewChild('room_link') roomLink;
  @ViewChild('room_map') roomMap;
  @ViewChild('room_number') roomNumber;
  @ViewChild('room_desc') roomDesc;
  @ViewChild('room_sq_ft') roomSqFt;
  @ViewChild('roomEntryForm') roomEntryForm;

  public photos: any;
  public path: string;
  private rowId: number;
  private floorList: any;
  private primaryUseList: any;
  private selectedFloor: any;
  private selectedPrimaryUse: any;
  private loadingView: any;
  private isFloorLoading: boolean;
  private isPrimaryUseLoading: boolean;

  public data = {
    rowId: 0,
    floor: '',
    primary_use: '',
    room_area: '',
    room_description: '',
    room_link: '',
    room_map: '',
    room_name: '',
    room_number: '',
    room_square_footage: '',
    photos: []
  };

  ngOnInit() {
    this.photos = [];
    this.floorList = [];
    this.primaryUseList = [];
    this.isFloorLoading = true;
    this.isPrimaryUseLoading = true;
  }

  constructor(public navCtrl: NavController,
              private navParams: NavParams,
              private camera: Camera,
              private picker: ImagePicker,
              private file: File,
              private apiFactory: ApiFactoryProvider,
              private database: DatabaseProvider,
              private loading: LoadingController,
              private alertCtrl: AlertController) {

    this.rowId = this.navParams.get("rowId");

    if (this.rowId) {
      this.showExisting(this.rowId);
    }


    this.database.readToken().then((result: any) => {
      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;

        this.getFloors(accessToken);
        this.getPrimaryUse(accessToken);
      }
    }, (error) => {
      this.navCtrl.push(LoginPM360Page);
    });
  }

  public onChange(selectedValue) {
    // alert(this.selectedFloor);
  }

  private getFloors(accessToken: string) {

    // this.initLoader();
    // this.loadingView.present();

    this.isFloorLoading = true;


    this.apiFactory.getData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_floor",
      accessToken)
      .then(
        response => JSON.parse(response.data),
        error => {
          this.loadOfflineFloor();
          //this.getPrimaryUse();
        })
      .then((response: any) => {

        try {
          this.floorList = response.result;
          this.log("successfully pulled floor");
          this.database.saveOrReplaceData(
            JSON.stringify(response.result),
            "GET",
            0,
            "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_floor",
            "FLOOR")
            .then((result) => {
              //this.getPrimaryUse();
              this.log("successfully saved floor");
              this.log("x_nfgll_pm360_floor saved");
              this.isFloorLoading = false;
            }, (error) => {
              //this.getPrimaryUse();
              this.log("failed to save floor");
              this.log("failed to save!!!");
              this.isFloorLoading = false;
            });
        } catch (e) {
          this.loadOfflineFloor();
          //this.getPrimaryUse();
        }

      }, (error) => {
        this.loadOfflineFloor();
        //this.getPrimaryUse();
      })


  }

  private loadOfflineFloor() {
    this.database.readOfflineData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_floor", "GET", "FLOOR")
      .then((result: any) => {

        if (result.rows.length > 0) {
          this.floorList = JSON.parse(result.rows.item(0).data);
        }
        this.isFloorLoading = false;

      }, (error) => {
        this.isFloorLoading = false;
        this.log("ERROR " + JSON.stringify(error));
      });
  }

  private getPrimaryUse(accessToken: string) {

    this.isPrimaryUseLoading = true;

    this.apiFactory.getData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_primary_use",
      accessToken)
      .then(response => JSON.parse(response.data), (error) => {
        this.loadOfflinePrimaryUse();
      })
      .then((response: any) => {

        try {
          this.primaryUseList = response.result;

          this.database.saveOrReplaceData(JSON.stringify(response.result),
            "GET",
            0,
            "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_primary_use",
            "PRIMARY USE")
            .then((result) => {
              //this.loadingView.dismiss();
              this.log("x_nfgll_pm360_primary_use saved");
              this.isPrimaryUseLoading = false;
            }, (error) => {
              //this.loadingView.dismiss();
              this.isPrimaryUseLoading = false;
              this.log("failed to save!!!");
            });
        } catch (e) {
          this.log("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_primary_use - ERROR -" + JSON.stringify(e));
          this.loadOfflinePrimaryUse();
        }

      }, (error) => {
        this.log(JSON.stringify(error));
        this.loadOfflinePrimaryUse();
      })

  }

  private loadOfflinePrimaryUse() {
    this.database.readOfflineData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_primary_use", "GET", "PRIMARY USE")
      .then((result: any) => {
        //this.loadingView.dismiss();

        if (result.rows.length > 0) {
          this.primaryUseList = JSON.parse(result.rows.item(0).data);
        }
        this.isPrimaryUseLoading = false;
      }, (error) => {
        //this.loadingView.dismiss();
        this.log("ERROR " + JSON.stringify(error));
        this.isPrimaryUseLoading = false;

      });
  }

  public goToRoom(params) {

    let data = {
      "floor": this.selectedFloor,
      "primary_use": this.selectedPrimaryUse,
      "room_area": this.roomArea.value,
      "room_description": this.roomDesc.value,
      "room_link": this.roomLink.value,
      "room_map": this.roomMap.value,
      "room_name": this.roomName.value,
      "room_number": this.roomNumber.value,
      "room_square_footage": this.roomSqFt.value,
      'photos1': this.photos
    };

    this.saveData(data);
  }

  public takePhoto(params) {

    const options: CameraOptions = {
      quality: 100,
      allowEdit: false,
      saveToPhotoAlbum: true,
      correctOrientation: true,
      encodingType: this.camera.EncodingType.JPEG,
      destinationType: this.camera.DestinationType.FILE_URI
    };

    this.camera.getPicture(options).then((imageData) => {

      let filename = imageData.substring(imageData.lastIndexOf('/') + 1);
      let filePath = imageData.substring(0, imageData.lastIndexOf('/') + 1);

      this.file.readAsDataURL(filePath, filename).then((base64res) => {
          this.photos.push(base64res);
          this.photos.reverse();
        }, (error) => {
          alert(error);
        }
      );
    }, (error) => {
      alert(error);
    });
  }

  public pickPhoto(params) {

    let option = {
      title: 'Select Picture',
      message: 'Select Picture',
      maximumImagesCount: 1,
      outType: 0
    };

    this.picker.getPictures(option).then((imageData) => {
      let filename = imageData.substring(imageData.lastIndexOf('/') + 1);
      let filePath = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      this.log("IMAGE PATH GET PATH. -> " + filePath);

      this.file.readAsDataURL(filePath, filename).then((base64res) => {
          this.photos.push(base64res);
          this.photos.reverse();
        }
      );
    }, (error) => {
    });
  }

  public saveData(data: {}) {
    this.initLoader();
    this.loadingView.present();

    this.database.readToken().then((result: any) => {
      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;

        this.apiFactory.postData(
          "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_room",
          data,
          accessToken, 0).then(
          (response) => {
            this.loadingView.dismiss();
            //TODO upload file
            this.log("Callback SUCCESS POST");
          },
          (error) => {
            this.log("Callback ERROR => " + JSON.stringify(error));
            this.saveToDB(data);
          }).then(
          (response) => {
            this.loadingView.dismiss();
            alert("Uploaded Successfully!!!");
          },
          (error) => {
            this.log("in promise error " + JSON.stringify(error));
            this.saveToDB(data);
          });
      }
    }, (error) => {
      this.loadingView.dismiss();
      this.log(JSON.stringify(error));
    });
  }

  private showExisting(rowId: number) {
    // this.initLoader();
    // this.loadingView.present();

    this.database.readDataById("ROOM", rowId).then((result: any) => {
      if (result.rows.length > 0) {

        var row = JSON.parse(result.rows.item(0).data);

        this.data.rowId = row.rowid;
        this.data.floor = row.floor;
        this.data.primary_use = row.primary_use;
        this.data.room_area = row.room_area;
        this.data.room_description = row.room_description;
        this.data.room_link = row.room_link;
        this.data.room_map = row.room_map;
        this.data.room_name = row.room_name;
        this.data.room_number = row.room_number;
        this.data.room_square_footage = row.room_square_footage;
        this.data.photos = row.photos;
        this.photos = row.photos;
      }
      // //this.loadingView.dismiss();

    }, (error) => {
      // //this.loadingView.dismiss();
    });
  }

  private saveToDB(data: {}) {
    if (!this.rowId) {
      this.database.saveData(
        JSON.stringify(data),
        "POST",
        0,
        "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_room",
        "ROOM")

        .then((result) => {
            this.loadingView.dismiss();
            alert("Saved Successfully!!!");
            this.navCtrl.push(RoomListPage);
          },
          (error) => {
            this.loadingView.dismiss();
          });
    } else {
      this.database.updateData(
        JSON.stringify(data),
        "POST",
        0,
        "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_room",
        "ROOM",
        this.rowId)

        .then((result) => {
            this.loadingView.dismiss();
            alert("Updated Successfully!!!");
            this.navCtrl.push(RoomListPage);
          },
          (error) => {
            this.loadingView.dismiss();
          });
    }
  }

  public deleteRoom(rowId: number) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to remove this entry?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            alert.dismiss();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.database.deleteData(rowId).then(() => {
              this.log("deleted");
              alert.dismiss();
              this.navCtrl.push(RoomListPage);
            });
          }
        }
      ]
    });
    alert.present();
  }

  private initLoader() {

    this.loadingView = this.loading.create({
      spinner: 'dots',
      content: 'Please wait...'
    });

  }

  public deletePhoto(position) {
    alert("Clicked to Remove Index " + position);
  }

  private log(message: string) {
    console.log(message);
  }
}
