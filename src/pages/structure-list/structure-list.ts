import {Component} from "@angular/core";
import {IonicPage, LoadingController, NavController, NavParams} from "ionic-angular";
import {DatabaseProvider} from "../../providers/database/database";
import {AddStructurePage} from "../add-structure/add-structure";
import {ApiFactoryProvider} from "../../providers/api-factory/api-factory";

/**
 * Generated class for the StructureListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-structure-list',
  templateUrl: 'structure-list.html',
})
export class StructureListPage {

  private structures: any = [];
  private structureData: any = [];
  private loadingView: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public apiFactory: ApiFactoryProvider,
              private database: DatabaseProvider,
              private loading: LoadingController) {
  }

  ionViewDidLoad() {
    this.readData();
  }

  ionViewWillEnter() {
    this.readData();
  }

  readData() {

    this.database.readData("STRUCTURE", "POST").then((res: any) => {
      this.structures = [];
      this.structureData = res.rows;
      for (var i = 0; i < res.rows.length; i++) {

        var data = JSON.parse(res.rows.item(i).data);

        this.structures.push({
          rowId: res.rows.item(i).rowid,
          structure_name: data.structure_name,
          structure_city: data.structure_city,
          structure_description: data.structure_description
        });
      }
    }, (error) => {

    });

  }

  syncStructure() {
    var dataToSync = [];

    for (var i = 0; i < this.structureData.length; i++) {

      var data = JSON.parse(this.structureData.item(i).data);

      dataToSync.push({
        rowId: this.structureData.item(i).rowid,
        data: data,
        url: this.structureData.item(i).syncUrl,
      });
    }

    this.database.readToken().then((result: any) => {

      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;

        const total = dataToSync.length;
        this.initLoader(0, total);
        this.syncData(dataToSync,
          0,
          total,
          accessToken);

      }
    }, (error) => {

    });

  }

  syncData(dataToSync: any, currentIndex: number, totalSize: number, token: string) {
    this.log("Recursive call --- index " + currentIndex + " total " + totalSize);
    if (currentIndex >= totalSize) {
      this.log("STOP RECURSION");
      this.loadingView.dismiss();
      this.readData();
      return
    } else {
      this.apiFactory.postData(
        dataToSync[currentIndex].url,
        dataToSync[currentIndex].data,
        token,
        dataToSync[currentIndex].rowId)
        .then((rowIdAsResult: number) => {

            this.database.deleteData(rowIdAsResult).then((rowIdAsResult: number) => {
                currentIndex++;
                this.loadingView.dismiss();

                if (currentIndex >= totalSize) {
                  this.readData();
                  return;
                }
                else {
                  this.initLoader(currentIndex, totalSize);
                  this.syncData(dataToSync, currentIndex, totalSize, token);
                  this.log("DELETED rowid: " + rowIdAsResult);

                }

              },
              (error) => {
                this.loadingView.dismiss();
                this.log("in promise error " + JSON.stringify(error));
              });
          },
          (error) => {
            this.loadingView.dismiss();
            this.log("in promise error " + JSON.stringify(error));
          });
    }
  }

  private initLoader(current: number, total: number) {

    this.loadingView = this.loading.create({
      spinner: 'dots',
      content: "Syncing... (" + (current + 1) + "/" + total + ")"
    });
    this.loadingView.present();

  }

  editStructure(rowId) {
    this.navCtrl.push(AddStructurePage, {
      rowId: rowId
    });
  }

  private log(message: string) {
    console.log(message);
  }
}
