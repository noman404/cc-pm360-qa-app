import {Component, ViewChild} from "@angular/core";
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from "ionic-angular";
import {DatabaseProvider} from "../../providers/database/database";
import {StructureListPage} from "../structure-list/structure-list";
import {ApiFactoryProvider} from "../../providers/api-factory/api-factory";
import {LoginPM360Page} from "../login-pm360/login-pm360";

/**
 * Generated class for the AddStructurePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-structure',
  templateUrl: 'add-structure.html',
})
export class AddStructurePage {

  @ViewChild('structureName') structureName;
  @ViewChild('structureDescription') structureDescription;
  @ViewChild('structureSqFt') structureSqFt;
  @ViewChild('structureCity') structureCity;
  @ViewChild('structureZipCode') structureZipCode;
  @ViewChild('structureMailAddress') structureMailAddress;

  private rowId: number;
  private locationList: any;
  private stateList: any;
  private structureTypeList: any;
  private loadingView: any;
  private selectedStructureType: any;
  private selectedLocation: any;
  private selectedState: any;

  private isLocationLoading: boolean;
  private isStateLoading: boolean;
  private isStructureTypeLoading: boolean;

  public data = {
    rowId: 0,
    structure_description: "",
    structure_square_footage: '',
    structure_city: "",
    structure_zip_code: "",
    structure_mail_address: "",
    structure_name: "",
    structure_type: "",
    structure_location: "",
    structure_state: "",
  };


  ngOnInit() {
    this.locationList = [];
    this.stateList = [];
    this.structureTypeList = [];

    this.isLocationLoading = true;
    this.isStateLoading = true;
    this.isStructureTypeLoading = true;
  }

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private apiFactory: ApiFactoryProvider,
              private database: DatabaseProvider,
              private loading: LoadingController,
              private alertCtrl: AlertController) {

    this.rowId = this.navParams.get("rowId");
    if (this.rowId) {
      this.showExisting(this.rowId);
    }

    this.database.readToken().then((result: any) => {
      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;

        this.getLocationList(accessToken);
        this.getStateList(accessToken);
        this.getStructureTypeList(accessToken);
      }
    }, (error) => {
      this.navCtrl.push(LoginPM360Page);
    });
  }

  deleteStructure(rowId: number) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to remove this entry?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            alert.dismiss();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.database.deleteData(rowId).then(() => {
              this.log("deleted");
              alert.dismiss();
              this.navCtrl.push(StructureListPage);
            });
          }
        }
      ]
    });
    alert.present();
  }

  private initLoader() {

    this.loadingView = this.loading.create({
      spinner: 'dots',
      content: 'Please wait...'
    });

  }

  onChange(selectedValue) {
    // alert(this.selectedFloor);
  }

  ionViewDidLoad() {

  }

  saveStructure(params) {

    let data = {
      "structure_description": this.structureDescription.value,
      "structure_square_footage": this.structureSqFt.value,
      "structure_city": this.structureCity.value,
      "structure_zip_code": this.structureZipCode.value,
      "structure_mail_address": this.structureMailAddress.value,
      "structure_name": this.structureName.value,
      "structure_type": this.selectedStructureType,
      "structure_location": this.selectedLocation,
      "structure_state": this.selectedState
    };

    this.saveData(data);
  }

  saveData(data: {}) {
    this.initLoader();
    this.loadingView.present();
    this.database.readToken().then((result: any) => {
      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;
        this.apiFactory.postData(
          "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_structure",
          data,
          accessToken, 0).then(
          response => {
            this.loadingView.dismissAll();
            this.log("Callback SUCCESS POST")
          },
          error => {
            this.log("Callback ERROR => " + JSON.stringify(error));
            this.saveToDB(data);
          }).then(
          (response) => {
            this.loadingView.dismissAll();
            alert("Uploaded Successfully!!!");
          },
          (error) => {
            this.log("in promise error " + JSON.stringify(error));
            //alert("ERROR PROMISE " + JSON.stringify(error));
            this.saveToDB(data);
          });
      }
    }, (error) => {
      this.loadingView.dismissAll();
      this.log(JSON.stringify(error));
    });
  }

  private showExisting(rowId: number) {
    this.database.readDataById("STRUCTURE", rowId).then((result: any) => {
      if (result.rows.length > 0) {

        var row = JSON.parse(result.rows.item(0).data);

        this.data.rowId = row.rowid;
        this.data.structure_description = row.structure_description;
        this.data.structure_square_footage = row.structure_square_footage;
        this.data.structure_city = row.structure_city;
        this.data.structure_zip_code = row.structure_zip_code;
        this.data.structure_mail_address = row.structure_mail_address;
        this.data.structure_name = row.structure_name;
        this.data.structure_type = row.structure_type;
        this.data.structure_location = row.structure_location;
        this.data.structure_state = row.structure_state;

      }

    }, (error) => {

    });
  }

  private getLocationList(accessToken: string) {

    // this.initLoader();
    // this.loadingView.present();
    this.isLocationLoading = true;


    this.apiFactory.getData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_location", accessToken)
      .then(
        response => JSON.parse(response.data),
        error => {
          this.loadOfflineLocation();
          //this.getStateList();
        })
      .then((response: any) => {

        if (response.result !== undefined) {
          this.locationList = response.result;

          this.database.saveOrReplaceData(JSON.stringify(this.locationList),
            "GET",
            0,
            "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_location",
            "LOCATION")
            .then((result) => {
              this.isLocationLoading = false;

              this.log("x_nfgll_pm360_primary_use saved");
            }, (error) => {
              this.isLocationLoading = false;

            });
        } else {

          this.loadOfflineLocation();
          //this.getStateList();
        }

      }, (error) => {
        this.loadOfflineLocation();
        //this.getStateList();

      })

  }

  private loadOfflineLocation() {
    this.database.readOfflineData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_location", "GET", "LOCATION")
      .then((result: any) => {
        if (result.rows.length > 0) {
          this.locationList = JSON.parse(result.rows.item(0).data);
        }
        this.isLocationLoading = false;

      }, (error) => {
        this.log("ERROR " + JSON.stringify(error));
        this.isLocationLoading = false;

      });
  }

  private getStateList(accessToken: string) {
    this.isStateLoading = true;

    this.apiFactory.getData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_us_state",
      accessToken)
      .then(response => JSON.parse(response.data), error => {

        this.loadOfflineState();
        //this.getStructureTypeList();

      })
      .then((response: any) => {

        if (response.result !== undefined) {
          this.stateList = response.result;
          this.database.saveOrReplaceData(JSON.stringify(this.stateList),
            "GET",
            0,
            "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_us_state",
            "STATE")
            .then((result) => {
              //this.getStructureTypeList();
              this.isStateLoading = false;
              this.log("x_nfgll_pm360_primary_use saved");
            }, (error) => {
              this.isStateLoading = false;

              this.log("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_us_state - ERROR -" + JSON.stringify(error));

              //this.getStructureTypeList();

            });
        } else {
          this.loadOfflineState();
          //this.getStructureTypeList();
        }

      }, (error) => {
        this.loadOfflineState();
        //this.getStructureTypeList();
      })

  }

  private loadOfflineState() {
    this.database.readOfflineData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_us_state", "GET", "STATE")
      .then((result: any) => {
        if (result.rows.length > 0) {
          this.stateList = JSON.parse(result.rows.item(0).data);
        }
        this.isStateLoading = false;

      }, (error) => {
        //alert("ERROR " + JSON.stringify(error));
        this.isStateLoading = false;

      });
  }

  private getStructureTypeList(accessToken: string) {
    this.isStructureTypeLoading = true;
    this.apiFactory.getData("https://concourseqa.service-now.com/api/now/table/u_imp_tmpl_x_nfgll_pm360_structure_type",
      accessToken)
      .then(response => JSON.parse(response.data), error => {
        this.loadOfflineStructureType();
      })
      .then((response: any) => {

        if (response.result !== undefined) {
          this.structureTypeList = response.result;

          this.database.saveOrReplaceData(JSON.stringify(this.structureTypeList),
            "GET",
            0,
            "https://concourseqa.service-now.com/api/now/table/u_imp_tmpl_x_nfgll_pm360_structure_type",
            "STRUCTURE TYPE")
            .then((result) => {
              //this.loadingView.dismissAll();
              this.structureTypeList = false;
              this.log("x_nfgll_pm360_primary_use saved");
            }, (error) => {
              this.log("https://concourseqa.service-now.com/api/now/table/u_imp_tmpl_x_nfgll_pm360_structure_type - ERROR -" + JSON.stringify(error));
              this.structureTypeList = false;

              //this.loadingView.dismissAll();

            });
        } else {
          this.loadOfflineStructureType();

        }
      }, (error) => {
        this.loadOfflineStructureType();
      })

  }

  private loadOfflineStructureType() {
    this.database.readOfflineData("https://concourseqa.service-now.com/api/now/table/u_imp_tmpl_x_nfgll_pm360_structure_type", "GET", "STRUCTURE TYPE")
      .then((result: any) => {
        //this.loadingView.dismissAll();

        if (result.rows.length > 0) {

          this.structureTypeList = JSON.parse(result.rows.item(0).data);
        }
        this.structureTypeList = false;

      }, (error) => {
        //this.loadingView.dismissAll();
        this.structureTypeList = false;

        this.log("ERROR " + JSON.stringify(error));
      });
  }

  private saveToDB(data: {}) {
    if (!this.rowId) {
      this.database.saveData(JSON.stringify(data),
        "POST",
        0,
        "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_structure",
        "STRUCTURE")
        .then((result) => {
          this.loadingView.dismissAll();

          alert("Saved Successfully!!!");
          this.navCtrl.push(StructureListPage);
        }, (error) => {
          this.loadingView.dismissAll();

        });
    } else {
      this.database.updateData(
        JSON.stringify(data),
        "POST",
        0,
        "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_structure",
        "STRUCTURE",
        this.rowId)
        .then((result) => {
          this.loadingView.dismissAll();

          alert("Updated Successfully!!!");
          this.navCtrl.push(StructureListPage);
        }, (error) => {
          this.loadingView.dismissAll();

        });
    }
  }


  private log(message: string) {
    console.log(message);
  }

}
