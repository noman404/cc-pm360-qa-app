import {Component} from "@angular/core";
import {LoadingController, NavController, NavParams} from "ionic-angular";
import {DatabaseProvider} from "../../providers/database/database";
import {AddFloorPage} from "../add-floor/add-floor";
import {ApiFactoryProvider} from "../../providers/api-factory/api-factory";

/**
 * Generated class for the FloorListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-floor-list',
  templateUrl: 'floor-list.html',
})
export class FloorListPage {

  private floors: any = [];
  private floorData: any = [];
  private loadingView: any;
  private offlineLoadingView: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public apiFactory: ApiFactoryProvider,
              private database: DatabaseProvider,
              private loading: LoadingController) {
  }

  private initLoader(current: number, total: number) {

    this.loadingView = this.loading.create({
      spinner: 'dots',
      content: "Syncing... (" + (current + 1) + "/" + total + ")"
    });
    this.loadingView.present();

  }

  private initLoading() {

    /*this.offlineLoadingView = this.loading.create({
      spinner: 'dots',
      content: "Preparing Offline Data..."
    });
    this.offlineLoadingView.present();*/

  }


  ionViewDidLoad() {
    this.readData();
  }

  ionViewWillEnter() {
    this.readData();
  }

  readData() {
    this.initLoading();

    this.database.readData("FLOOR", "POST").then((res: any) => {
      this.floors = [];

      this.floorData = res.rows;

      for (var i = 0; i < res.rows.length; i++) {

        var data = JSON.parse(res.rows.item(i).data);
        this.floors.push({
          rowId: res.rows.item(i).rowid,
          floor_name: data.floor_name,
          floor_number: data.floor_number,
          floor_description: data.floor_description
        });
      }
      // this.offlineLoadingView.dismiss();

    }, (error) => {
      // this.offlineLoadingView.dismiss();
      this.log("in promise db read error " + JSON.stringify(error));
    });
  }

  editFloor(rowId) {
    this.navCtrl.push(AddFloorPage, {
      rowId: rowId
    });
  }

  syncFloor() {
    //read db to fetch all unsynced floor data
    //sync each
    //remove synced
    //update UI

    var dataToSync = [];

    for (var i = 0; i < this.floorData.length; i++) {

      var data = JSON.parse(this.floorData.item(i).data);

      dataToSync.push({
        rowId: this.floorData.item(i).rowid,
        data: data,
        url: this.floorData.item(i).syncUrl,
      });
    }

    this.database.readToken().then((result: any) => {

      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;

        const total = dataToSync.length;
        this.initLoader(0, total);
        this.syncData(dataToSync,
          0,
          total,
          accessToken);

      }
    }, (error) => {

    });

  }

  syncData(dataToSync: any, currentIndex: number, totalSize: number, token: string) {
    this.log("Recursive call --- index " + currentIndex + " total " + totalSize);
    if (currentIndex >= totalSize) {
      this.log("STOP RECURSION");
      this.loadingView.dismiss();
      this.readData();
      return
    } else {
      this.apiFactory.postData(
        dataToSync[currentIndex].url,
        dataToSync[currentIndex].data,
        token,
        dataToSync[currentIndex].rowId)
        .then((rowIdAsResult: number) => {

            this.database.deleteData(rowIdAsResult).then((rowIdAsResult: number) => {

                currentIndex++;

                this.loadingView.dismiss();

                if (currentIndex >= totalSize) {
                  this.readData();
                  return;
                }
                else {
                  this.initLoader(currentIndex, totalSize);
                  this.syncData(dataToSync, currentIndex, totalSize, token);
                  this.log("DELETED rowid: " + rowIdAsResult);

                }

              },
              (error) => {
                this.loadingView.dismiss();
                this.log("in promise error " + JSON.stringify(error));
              });
          },
          (error) => {
            this.loadingView.dismiss();
            this.log("in promise error " + JSON.stringify(error));
          });
    }
  }

  private log(message: string) {
    console.log(message);
  }

}
