import {Component} from "@angular/core";
import {IonicPage, LoadingController, NavController, NavParams} from "ionic-angular";
import {DatabaseProvider} from "../../providers/database/database";
import {AddAssetPM360Page} from "../add-asset-pm360/add-asset-pm360";
import {ApiFactoryProvider} from "../../providers/api-factory/api-factory";

/**
 * Generated class for the AssetListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-asset-list',
  templateUrl: 'asset-list.html',
})
export class AssetListPage {

  private assets: any = [];
  private assetsData: any = [];
  private loadingView: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public apiFactory: ApiFactoryProvider,
              private database: DatabaseProvider,
              private loading: LoadingController) {
  }

  ionViewDidLoad() {
    this.readData();
  }

  ionViewWillEnter() {
    this.readData();
  }

  readData() {

    this.database.readData("ASSET", "POST").then((result: any) => {
      this.assets = [];
      this.assetsData = result.rows;
      for (var i = 0; i < result.rows.length; i++) {

        var data = JSON.parse(result.rows.item(i).data);

        this.assets.push({
          rowId: result.rows.item(i).rowid,
          asset_name: data.asset_name,
          asset_number: data.asset_number,
          asset_description: data.asset_description
        });
      }
    }, (error) => {

    });

  }

  syncAsset() {
    var dataToSync = [];

    // this.database.readData("FLOOR", "POST").then((res: any) => {
    //   dataToSync = [];
    for (var i = 0; i < this.assetsData.length; i++) {

      var data = JSON.parse(this.assetsData.item(i).data);

      dataToSync.push({
        rowId: this.assetsData.item(i).rowid,
        data: data,
        url: this.assetsData.item(i).syncUrl,
      });
    }


    this.database.readToken().then((result: any) => {

      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;

        const total = dataToSync.length;
        this.initLoader(0, total);

        //TODO upload images and prepare list of their access id first
        this.syncData(dataToSync,
          0,
          total,
          accessToken);
      }
    }, (error) => {

    });

  }

  syncData(dataToSync: any, currentIndex: number, totalSize: number, token: string) {

    this.log("Recursive call --- index " + currentIndex + " total " + totalSize);

    if (currentIndex >= totalSize) {
      this.log("STOP RECURSION");
      this.loadingView.dismiss();
      this.readData();
      return;
    } else {
      this.apiFactory.postData(
        dataToSync[currentIndex].url,
        dataToSync[currentIndex].data,
        token,
        dataToSync[currentIndex].rowId)
        .then((rowIdAsResult: number) => {

            this.database.deleteData(rowIdAsResult).then((rowIdAsResult: number) => {

                currentIndex++;

                this.loadingView.dismiss();

                if (currentIndex >= totalSize) {
                  this.readData();
                  return;
                }
                else {
                  this.initLoader(currentIndex, totalSize);
                  this.syncData(dataToSync, currentIndex, totalSize, token);
                  this.log("DELETED rowid: " + rowIdAsResult);

                }

              },
              (error) => {
                this.loadingView.dismiss();
                this.log("in promise error " + JSON.stringify(error));
              });
          },
          (error) => {
            this.loadingView.dismiss();
            this.log("in promise error " + JSON.stringify(error));
          });
    }
  }

  private initLoader(current: number, total: number) {

    this.loadingView = this.loading.create({
      spinner: 'dots',
      content: "Syncing... (" + (current + 1) + "/" + total + ")"
    });
    this.loadingView.present();

  }


  editAsset(rowId) {
    this.navCtrl.push(AddAssetPM360Page, {
      rowId: rowId
    });
  }


  private log(message: string) {
    console.log(message);
  }


}
