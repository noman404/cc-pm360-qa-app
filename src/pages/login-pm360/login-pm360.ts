import {Component, ViewChild} from "@angular/core";
import {LoadingController, NavController} from "ionic-angular";
import {ApiFactoryProvider} from "../../providers/api-factory/api-factory";
import {AddRoomPM360Page} from "../add-room-pm360/add-room-pm360";
import {DatabaseProvider} from "../../providers/database/database";
import {Storage} from "@ionic/Storage";

@Component({
  selector: 'page-login-pm360',
  templateUrl: 'login-pm360.html'
})
export class LoginPM360Page {

  @ViewChild('username') userName;
  @ViewChild('password') password;

  private rememberMe: boolean;

  private loadingView: any;

  constructor(public navCtrl: NavController,
              private database: DatabaseProvider,
              private apiFactory: ApiFactoryProvider,
              private loading: LoadingController,
              private prefStorage: Storage) {

    this.getRememberMe();
  }

  private getRememberMe() {

    this.prefStorage.get("remember_me").then((value: number) => {
      this.log("Remember Me Get on Load " + value);
      if (value == null) {
        this.rememberMe = false;
      } else {
        this.rememberMe = (value == 1);
        // if (this.rememberMe) {
        // this.navCtrl.setRoot(AddRoomPM360Page).then(() => {
        //   this.log("Remember Me True rootview changed");
        // });
        // }
      }
      this.log("Remember Me Get " + this.rememberMe);
    }, (error) => {
      this.rememberMe = false;
      this.log("Remember Me Get Error " + this.rememberMe);
    });
  }

  updateRememberMe() {
  }

  goToAddRoomPM360(params) {
    this.initLoader();
    this.loadingView.present();

    this.apiFactory.getToken(this.userName.value, this.password.value)
      .then(response => JSON.parse(response.data),
        error => {
          this.loadingView.dismiss();
          this.log("ERROR => " + JSON.stringify(error));
        })
      .then((response: any) => {
        this.log("TOKEN: " + response.access_token);

        this.database.saveToken(response.access_token, response.refresh_token)
          .then((result) => {
            this.log("TOKEN SAVE CALLBACK "+JSON.stringify(result));

           /* this.apiFactory.getData(
              "https://concourseprod.service-now.com/api/now/table/x_pm_360_storage?sysparm_query=active=true",
              "1mup6bJGtWSlPnD866IDpN7-sv7OwX9uqJk3_FG4i0LxGcWDq7VbPH-mvs1hdXfivERiQCpS3toR8nzDEX087A")
              .then(
                response => JSON.parse(response.data),
                error => {
                  this.log("ERROR => " + JSON.stringify(error));
                })
              .then((response: any) => {

                this.log("Active Value " + response.result[0].active + " Vendor " + response.result[0].vendor);
                // this.loadingView.dismiss();
                // alert("Login Successful!!");

                this.prefStorage.set("storage_option", response.result[0].vendor).then((success: any) => {
*/
                  this.loadingView.dismiss();
                  alert("Login Successful");

                  this.navCtrl.push(AddRoomPM360Page)
                    .then(() => {

                      if (this.rememberMe) {
                        this.log("Remember Me Set " + this.rememberMe);
                        this.prefStorage.set("remember_me", this.rememberMe == true ? 1 : 0);
                        this.prefStorage.get("remember_me").then((value: number) => {
                          this.log("Remember Me Get After Set " + value);
                        }, (error) => {
                          this.log("Remember Me Get Error " + JSON.stringify(error));
                        });

                      }
                      this.navCtrl.setRoot(AddRoomPM360Page).then(() => {
                        this.log("rootView changed");
                      });

                    /*}, (error) => {
                      this.loadingView.dismiss();
                      this.log("Nav Error " + JSON.stringify(error));
                    });


                }, (error) => {
                  this.loadingView.dismiss();
                  this.log("Store Set Error " + JSON.stringify(error));

                });*/

              }, (error) => {
                this.log("Storage Error " + JSON.stringify(error));
                this.loadingView.dismiss();
              });

          }, (error) => {
            this.loadingView.dismiss();
            this.log("DB TOKEN ERROR => " + JSON.stringify(error));
          }).catch((error) => {
          this.loadingView.dismiss();
          this.log("DB TOKEN ERROR => " + JSON.stringify(error));
        });
      }, (error) => {
        this.loadingView.dismiss();
        this.log("DB ERROR => " + JSON.stringify(error));
      });
  }

  goToRoom(params) {
    //TODO nothing
  }

  private initLoader() {

    this.loadingView = this.loading.create({
      spinner: 'dots',
      content: 'Authenticating...'
    });

  }

  private log(message: string) {
    console.log(message);
  }
}
