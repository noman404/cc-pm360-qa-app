import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {LoginPM360Page} from "../login-pm360/login-pm360";
import {AddRoomPM360Page} from "../add-room-pm360/add-room-pm360";
import {AddAssetPM360Page} from "../add-asset-pm360/add-asset-pm360";
import {RoomListPage} from "../room-list/room-list";
import {AssetListPage} from "../asset-list/asset-list";
import {StructureListPage} from "../structure-list/structure-list";
import {AddStructurePage} from "../add-structure/add-structure";
import {FloorListPage} from "../floor-list/floor-list";
import {AddFloorPage} from "../add-floor/add-floor";

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerPage {

  tab1Root: any = AddRoomPM360Page;
  tab2Root: any = AddAssetPM360Page;
  tab3Root: any = LoginPM360Page;
  tab4Root: any = RoomListPage;
  tab5Root: any = AssetListPage;
  tab6Root: any = AddStructurePage;
  tab7Root: any = StructureListPage;
  tab8Root: any = AddFloorPage;
  tab9Root: any = FloorListPage;

  constructor(public navCtrl: NavController) {
  }

  goToAddRoomPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(AddRoomPM360Page);
  }

  goToRoomListPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(RoomListPage);
  }

  goToAssetListPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(AssetListPage);
  }

  goToLoginPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(LoginPM360Page);
  }

  goToAddAssetPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(AddAssetPM360Page);
  }

  goToStructureListPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(StructureListPage);
  }

  goToFloorListPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(FloorListPage);
  }

  goToAddStructurePM360(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(AddStructurePage);
  }

  goToAddFloorPM360(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(AddFloorPage);
  }

}
