import {Component, ViewChild} from "@angular/core";
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from "ionic-angular";
import {FloorListPage} from "../floor-list/floor-list";
import {DatabaseProvider} from "../../providers/database/database";
import {ApiFactoryProvider} from "../../providers/api-factory/api-factory";
import {LoginPM360Page} from "../login-pm360/login-pm360";

/**
 * Generated class for the AddFloorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-floor',
  templateUrl: 'add-floor.html',
})
export class AddFloorPage {

  @ViewChild('floorName') floorName;
  @ViewChild('floorNumber') floorNumber;
  @ViewChild('floorDescription') floorDescription;
  @ViewChild('floorSqFt') floorSqFt;

  private selectedStructure: any;

  public data = {
    rowId: 0,
    floor_description: "",
    floor_square_footage: '',
    floor_number: "",
    floor_name: "",
    floor_structure: ""
  };

  private rowId: number;
  private structureList: any = [];
  private loadingView: any;
  private isLoading: boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private  apiFactory: ApiFactoryProvider,
              private database: DatabaseProvider,
              private loading: LoadingController,
              private alertCtrl: AlertController) {

    this.rowId = this.navParams.get("rowId");

    if (this.rowId) {
      this.showExisting(this.rowId);
    }

    this.database.readToken().then((result: any) => {
      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;

        this.getStructures(accessToken);
      }
    }, (error) => {
      this.navCtrl.push(LoginPM360Page);
    });
  }

  private initLoader() {

    this.loadingView = this.loading.create({
      spinner: 'dots',
      content: 'Please wait...'
    });

  }

  deleteFloor(rowId: number) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to remove this entry?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            alert.dismiss();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.database.deleteData(rowId).then(() => {
              this.log("deleted");
              alert.dismiss();
              this.navCtrl.push(FloorListPage);
            });
          }
        }
      ]
    });
    alert.present();
  }

  ngOnInit() {
    this.structureList = [];
    this.isLoading = true;
  }

  onChange(selectedValue) {
    // alert(this.selectedFloor);
  }

  showExisting(rowId: number) {
    // this.initLoader();
    // this.loadingView.present();

    this.database.readDataById("FLOOR", rowId).then(
      (result: any) => {
        if (result.rows.length > 0) {

          var row = JSON.parse(result.rows.item(0).data);

          this.data.rowId = row.rowid;
          this.data.floor_description = row.floor_description;
          this.data.floor_square_footage = row.floor_square_footage;
          this.data.floor_number = row.floor_number;
          this.data.floor_name = row.floor_name;
          this.data.floor_structure = row.floor_structure;
        }
        // this.loadingView.dismiss();
      }, (error) => {
        // this.loadingView.dismiss();
      });
  }

  saveFloor(params) {

    let data = {
      "floor_description": this.floorDescription.value,
      "floor_square_footage": this.floorSqFt.value,
      "floor_number": this.floorNumber.value,
      "floor_name": this.floorName.value,
      "floor_structure": this.selectedStructure
    };

    this.saveData(data);
  }

  saveToDB(data: {}) {
    if (this.rowId) {
      //TODO update data
      this.database.updateData(
        JSON.stringify(data),
        "POST",
        0,
        "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_floor",
        "FLOOR",
        this.rowId)
        .then((result) => {
          this.loadingView.dismiss();
          alert("Updated Successfully!!!");
          this.navCtrl.push(FloorListPage);
        }, (error) => {
          this.loadingView.dismiss();
        });

    } else {
      //TODO create data
      this.database.saveData(JSON.stringify(data),
        "POST",
        0,
        "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_floor",
        "FLOOR")

        .then((result) => {
          this.loadingView.dismiss();
          alert("Saved Successfully!!!");
          this.navCtrl.push(FloorListPage);
        }, (error) => {
          this.loadingView.dismiss();
        });
    }
  }

  saveData(data: {}) {
    this.initLoader();
    this.loadingView.present();

    this.database.readToken().then((result: any) => {
      if (result.rows.length > 0) {

        var accessToken = result.rows.item(0).access_token;
        var refreshToken = result.rows.item(0).refresh_token;

        this.apiFactory.postData(
          "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_floor",
          data,
          accessToken
          , 0).then(
          response => {
            this.loadingView.dismiss();
          },
          error => {
            this.log("Callback ERROR => " + JSON.stringify(error));
            this.saveToDB(data);
          }).then(
          (response) => {
            this.loadingView.dismiss();
            alert("Uploaded Successfully!!!");
          },
          (error) => {
            this.log("in promise error " + JSON.stringify(error));
            //alert("ERROR PROMISE " + JSON.stringify(error));
            this.saveToDB(data);
          });
      }
    }, (error) => {
      this.loadingView.dismiss();
      this.log(JSON.stringify(error));
    });
  }

  private getStructures(accessToken: string) {
    // this.initLoader();
    // this.loadingView.present();
    this.isLoading = true;


    this.apiFactory.getData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_structure",
      accessToken)
      .then(response => JSON.parse(response.data), error => {
        this.loadOfflineStructure();
      })
      .then((response: any) => {

        if (response.result !== undefined) {
          this.structureList = response.result;

          this.database.saveOrReplaceData(JSON.stringify(this.structureList),
            "GET",
            0,
            "https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_structure",
            "STRUCTURE")
            .then((result) => {
              // this.loadingView.dismiss();
              this.isLoading = false;
              this.log("x_nfgll_pm360_primary_use saved");
            }, (error) => {
              // this.loadingView.dismiss();
              this.isLoading = false;
            });
        } else {
          this.log("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_structure - ERROR -");
          this.loadOfflineStructure();
        }
      }, (error) => {
        this.loadOfflineStructure();
      })

  }

  private loadOfflineStructure() {
    this.database.readOfflineData("https://concourseqa.service-now.com/api/now/table/x_nfgll_pm360_structure", "GET", "STRUCTURE")
      .then((result: any) => {
        // this.loadingView.dismiss();
        this.isLoading = false;

        if (result.rows.length > 0) {
          this.structureList = JSON.parse(result.rows.item(0).data);
        }
      }, (error) => {
        // this.loadingView.dismiss();
        this.isLoading = false;

        this.log("ERROR " + JSON.stringify(error));
      });
  }


  private log(message: string) {
    console.log(message);
  }

}
