import {Injectable} from "@angular/core";
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";

/*
 Generated class for the DatabaseProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
 */
@Injectable()
export class DatabaseProvider {

  private syncTblSql: string;
  private authTblSql: string;

  //TODO types Sync Status
  // dirty -1
  // draft 1
  // read only 0

  constructor(public sqlite: SQLite) {

    this.syncTblSql = 'CREATE TABLE IF NOT EXISTS syncTbl(rowid INTEGER PRIMARY KEY AUTOINCREMENT, method TEXT, timestamp TEXT, data TEXT, status INT, syncUrl TEXT, type TEXT)';
    this.authTblSql = 'CREATE TABLE IF NOT EXISTS oauthTbl(rowid INTEGER PRIMARY KEY AUTOINCREMENT, access_token TEXT, refresh_token TEXT)';

  }

  saveData(data: string, method: string, syncStatus: number, syncUrl: string, type: string) {

    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db) => {

        db.executeSql(this.syncTblSql,
          [])
          .then(result => this.log('Create SQL Executed '))
          .catch(e => this.log('Executed SQL Failed: ' + e));

        let val = [
          method,
          new Date(),
          data,
          syncStatus,
          syncUrl,
          type];

        db.executeSql('INSERT INTO syncTbl(method,timestamp,data,status,syncUrl,type) VALUES(?,?,?,?,?,?)', val)
          .then(result => {
            resultCallback(result);
          })
          .catch(e => {
            errorCallback(e);
          });

      });
    });
  }

  saveOrReplaceData(data: string, method: string, syncStatus: number, syncUrl: string, type: string) {

    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db) => {

        db.executeSql(this.syncTblSql,
          [])
          .then(result => this.log('Create SQL Executed '))
          .catch(e => this.log('Executed SQL Failed: ' + e));

        let val = [
          method,
          new Date(),
          data,
          syncStatus,
          syncUrl,
          type];

        this.readByTypeUrlMethodData(type, syncUrl, method).then((result: any) => {

          if (result.rows.length > 0) {
            var row = JSON.parse(result.rows.item(0).data);
            var rowId = row.rowid;
            this.updateData(data, method, syncStatus, syncUrl, type, rowId).then((result) => {
              resultCallback(result);
            }, (error) => {
              errorCallback(error);
            });

          } else {

            // db.executeSql('INSERT INTO syncTbl(method,timestamp,data,status,syncUrl,type) VALUES(?,?,?,?,?,?)', val)
            this.saveData(data, method, syncStatus, syncUrl, type).then(result => {
              this.log("SAVE SUCCESSFUL");
              resultCallback(result);
            })
              .catch(e => {
                this.log("SAVE ERROR " + JSON.stringify(e));
                errorCallback(e);
              });
          }
        }, (error) => {
          this.log("UPDATE ERROR " + JSON.stringify(error));
          errorCallback(error);
        });
      });
    });
  }

  updateData(data: string, method: string, syncStatus: number, syncUrl: string, type: string, rowId: number) {

    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db) => {

        db.executeSql(this.syncTblSql,
          [])
          .then(res => this.log('Create SQL Executed '))
          .catch(e => this.log('Executed SQL Failed: ' + e));

        let val = [
          method,
          new Date(),
          data,
          syncStatus,
          syncUrl,
          type,
          rowId];

        db.executeSql('UPDATE syncTbl SET method=?,timestamp=?,data=?,status=?,syncUrl=?,type=? WHERE rowid=?', val).then(result => {
          resultCallback(result);
        })
          .catch(e => errorCallback(e));

      });
    });
  }

  readData(type: string, method: string) {

    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql(this.syncTblSql, [])
          .then(res => this.log('Create SQL Executed '))
          .catch(e => alert('Executed SQL Failed: ' + e));
        db.executeSql("SELECT * FROM syncTbl WHERE type = '" + type + "' AND method = '" + method + "' ORDER BY rowid DESC", [])
          .then(result => {
            resultCallback(result);
          })
          .catch(e => {
            this.log("db read error " + JSON.stringify(e));
            errorCallback(e);
          });
      }).catch(e => {
        this.log("db read error 2 " + JSON.stringify(e));
        errorCallback(e);
      });
    });
  }

  deleteData(rowId: number) {

    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        this.log("rowid " + rowId);
        db.executeSql('DELETE FROM syncTbl WHERE rowid=?', [rowId])
          .then(result => {
            resultCallback(rowId);
          }, error => {
            this.log('Delete SQL Failed: ' + JSON.stringify(error));
            errorCallback(error);
          });
      }).catch(e => {
        this.log('Executed SQL Failed: ' + e);
        errorCallback(e);
      });
    });
  }

  readByTypeUrlMethodData(type: string, syncUrl: string, method: string) {

    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql(this.syncTblSql, [])
          .then(res => this.log('Create SQL Executed '))
          .catch(e => alert('Executed SQL Failed: ' + e));
        db.executeSql("SELECT * FROM syncTbl WHERE type = '" + type + "' AND syncUrl = '" + syncUrl + "' AND method = '" + method + "' ORDER BY rowid DESC", [])
          .then(result => {
            resultCallback(result);
          })
          .catch(e => {
            errorCallback(e);
          });
      }).catch(e => {
        errorCallback(e);
      });
    });
  }

  readDataById(type: string, rowId: number) {

    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql(this.syncTblSql, [])
          .then(result => this.log('Create SQL Executed '))
          .catch(e => alert('Executed SQL Failed: ' + e));
        db.executeSql("SELECT * FROM syncTbl WHERE type='" + type + "' AND rowid=" + rowId, [])
          .then(result => {
            resultCallback(result);
          })
          .catch(e => errorCallback(e));
      }).catch(e => errorCallback(e));
    });
  }

  readOfflineData(syncUrl: string, method: string, type: string) {

    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql(this.syncTblSql, [])
          .then(result => this.log('Create SQL Executed '))
          .catch(e => alert('Executed SQL Failed: ' + e));
        db.executeSql("SELECT * FROM syncTbl WHERE syncUrl='" + syncUrl + "' AND method='" + method + "' AND type ='" + type + "'", [])
          .then(result => {
            resultCallback(result);
          })
          .catch(e => errorCallback(e));
      }).catch(e => errorCallback(e));
    });
  }

  saveToken(accessToken: string, refreshToken: string) {
    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db) => {
        db.executeSql("DROP TABLE IF EXISTS oauthTbl", []);
        db.executeSql(this.authTblSql, [])
          .then(res => this.log('Create SQL Executed '))
          .catch(e => this.log('Executed SQL Failed: ' + e));

        let val = [
          accessToken,
          refreshToken
        ];

        db.executeSql('INSERT INTO oauthTbl(access_token,refresh_token) VALUES(?,?)', val)
          .then(result => {
            resultCallback(result);
          })
          .catch(e => errorCallback(e));

      });
    });
  }

  updateToken(accessToken: string, refreshToken: string, rowId: number) {
    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db) => {

        db.executeSql(this.authTblSql, [])
          .then(res => this.log('Create SQL Executed '))
          .catch(e => this.log('Executed SQL Failed: ' + e));

        let val = [
          accessToken,
          refreshToken,
          rowId
        ];

        db.executeSql('UPDATE oauthTbl SET access_token=?,refresh_token=? WHERE rowid=?', val)
          .then(result => {
            alert("Updated Successfully!!!");
            resultCallback(result);
          })
          .catch(e => errorCallback(e));

      });
    });
  }

  readToken() {
    return new Promise((resultCallback, errorCallback) => {
      this.sqlite.create({
        name: 'pm360.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql(this.authTblSql, [])
          .then(result => this.log('Create SQL Executed '))
          .catch(e => alert('Executed SQL Failed: ' + e));
        db.executeSql("SELECT * FROM oauthTbl LIMIT 1", [])
          .then(result => {
            resultCallback(result);
          })
          .catch(e => errorCallback(e));
      }).catch(e => errorCallback(e));
    });
  }


  private log(message: string) {
    console.log(message);
  }

}
