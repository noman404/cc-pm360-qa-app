import {Injectable} from "@angular/core";
import {HTTP} from "@ionic-native/http";

/*
 Generated class for the ApiFactoryProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
 */
@Injectable()
export class ApiFactoryProvider {

  constructor(public http: HTTP) {
  }

  getData(url: string, token: string): Promise<any> {

    return new Promise((responseCallback, errorCallback) => {
      this.http.setRequestTimeout(3);
      this.http.get(url,
        {},
        {
          "Authorization": "Bearer " + token,
        })
        .then(result => {
          responseCallback(result);
          this.log(url + " - RESPONSE - " + JSON.stringify(result.data));
        })
        .catch(error => {
          this.log(url + " - ERROR - " + JSON.stringify(error));
          errorCallback(error);
        });
    });
  }

  postData(url: string, data: any, token: String, rowId: number) {
    let header = {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token,
    };
    return new Promise((responseCallback, errorCallback) => {
      this.http.setRequestTimeout(3);
      this.http.setDataSerializer('json');
      this.http.post(url, data, header).then(result => {
        this.log("post success");
        responseCallback(rowId);
      }, error => {
        this.log("onRejected -> " + JSON.stringify(error));
        errorCallback(error);
      }).catch(error => {
        this.log("onRejected Catch -> " + JSON.stringify(error));
        errorCallback(error);
      });
    });
  }

  getToken(username: string, password: string): Promise<any> {

    let body = {
      "grant_type": "password",
      "client_id": "ac0dd3408c1031006907010c2cc6ef6d",
      "client_secret": "67zoucbzudkp77yx9gs3",
      "username": username,
      "password": password
    };

    return new Promise((responseCallback, errorCallback) => {
      this.http.setRequestTimeout(3);
      this.http.post("https://concourseqa.service-now.com/oauth_token.do",
        body,
        {
          "Content-Type": "application/x-www-form-urlencoded",
        }).then(result => {
        responseCallback(result);
      })
        .catch(error => {
          errorCallback(error);
        });
    });
  }


  private log(message: string) {
    console.log(message);
  }

}
