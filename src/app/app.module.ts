import {ErrorHandler, NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {IonicApp, IonicErrorHandler, IonicModule} from "ionic-angular";
import {MyApp} from "./app.component";
import {LoginPM360Page} from "../pages/login-pm360/login-pm360";
import {AddRoomPM360Page} from "../pages/add-room-pm360/add-room-pm360";
import {AddAssetPM360Page} from "../pages/add-asset-pm360/add-asset-pm360";
import {TabsControllerPage} from "../pages/tabs-controller/tabs-controller";
import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";
import {HttpClientModule} from "@angular/common/http";
import {Camera} from "@ionic-native/camera";
import {ImagePicker} from "@ionic-native/image-picker";
import {SQLite} from "@ionic-native/sqlite";
import {RoomListPage} from "../pages/room-list/room-list";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {AssetListPage} from "../pages/asset-list/asset-list";
import {AddStructurePage} from "../pages/add-structure/add-structure";
import {StructureListPage} from "../pages/structure-list/structure-list";
import {AddFloorPage} from "../pages/add-floor/add-floor";
import {FloorListPage} from "../pages/floor-list/floor-list";
import {File} from "@ionic-native/file";
import {DatabaseProvider} from '../providers/database/database';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP} from "@ionic-native/http";
import { ApiFactoryProvider } from '../providers/api-factory/api-factory';
import { Network } from '@ionic-native/network';
import { IonicStorageModule } from '@ionic/Storage';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';


@NgModule({
  declarations: [
    MyApp,
    LoginPM360Page,
    AddRoomPM360Page,
    AddAssetPM360Page,
    TabsControllerPage,
    RoomListPage,
    AssetListPage,
    AddStructurePage,
    StructureListPage,
    AddFloorPage,
    FloorListPage,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPM360Page,
    AddRoomPM360Page,
    AddAssetPM360Page,
    TabsControllerPage,
    RoomListPage,
    AssetListPage,
    AddStructurePage,
    StructureListPage,
    AddFloorPage,
    FloorListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    ImagePicker,
    SQLite,
    BarcodeScanner,
    File,
    DatabaseProvider,
    HTTP,
    ApiFactoryProvider,
    Network,
    FileTransfer
  ]
})
export class AppModule {
}
