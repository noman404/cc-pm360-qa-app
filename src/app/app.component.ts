import {Component, ViewChild} from "@angular/core";
import {Nav, NavController, Platform} from "ionic-angular";
import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";

import {AddRoomPM360Page} from "../pages/add-room-pm360/add-room-pm360";
import {AddAssetPM360Page} from "../pages/add-asset-pm360/add-asset-pm360";

import {LoginPM360Page} from "../pages/login-pm360/login-pm360";
import {RoomListPage} from "../pages/room-list/room-list";
import {AssetListPage} from "../pages/asset-list/asset-list";
import {StructureListPage} from "../pages/structure-list/structure-list";
import {FloorListPage} from "../pages/floor-list/floor-list";
import {AddStructurePage} from "../pages/add-structure/add-structure";
import {AddFloorPage} from "../pages/add-floor/add-floor";
import {Storage} from "@ionic/Storage";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
  rootPage: any;
  private rememberMe: boolean;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private prefStorage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();


      this.prefStorage.get("remember_me").then((value: number) => {
        if (value == null) {
          this.rememberMe = false;
          this.rootPage = LoginPM360Page;
        } else {
          this.rememberMe = (value == 1);
          if (this.rememberMe) {
            this.rootPage = AddRoomPM360Page;
          } else {
            this.rootPage = LoginPM360Page;
          }
        }
      }, (error) => {
        this.rememberMe = false;
        this.rootPage = LoginPM360Page;
      });

    });
  }

  goToLoginPM360(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(LoginPM360Page);
  }

  goToLogoutPM360(params) {
    if (!params) params = {};
    this.prefStorage.set('remember_me', 0);
    // this.navCtrl.setRoot(AddRoomPM360Page).then(() => {
    //   this.log("Remember Me True rootview changed");
    // });
    this.navCtrl.setRoot(LoginPM360Page);
  }

  goToRoomListPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(RoomListPage);
  }

  goToAssetListPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(AssetListPage);
  }

  goToAddRoomPM360(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(AddRoomPM360Page);
  }

  goToAddAssetPM360(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(AddAssetPM360Page);
  }

  goToStructureListPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(StructureListPage);
  }

  goToFloorListPM360(params) {
    if (!params) params = {};
    this.navCtrl.push(FloorListPage);
  }

  goToAddStructurePM360(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(AddStructurePage);
  }

  goToAddFloorPM360(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(AddFloorPage);
  }
}
